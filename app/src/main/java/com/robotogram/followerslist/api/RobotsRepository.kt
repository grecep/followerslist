//package com.robotogram.followerslist.api
//
//import android.arch.lifecycle.LiveData
//import android.arch.lifecycle.MutableLiveData
//import com.robotogram.followerslist.data.Follower
//import com.robotogram.followerslist.data.models.User
//import com.robotogram.followerslist.helpers.schedulers.BaseSchedulerProvider
//import com.robotogram.followerslist.ui.Status
//import io.reactivex.disposables.CompositeDisposable
//
//class UsersRepository(private val schedulerProvider: BaseSchedulerProvider, private val usersApi: UsersApi) {
//
////    private val usersApi = UsersApi.instance
//    private val disposables = CompositeDisposable()
//
//    private var users: MutableLiveData<List<User>> = MutableLiveData()
//    private var status: MutableLiveData<Status> = MutableLiveData()
//
//    init {
//
//    }
//
//    // ok sam pol bi mogu se cleanat observable
//    fun getFollowers(user: String, currentFollowSlug: String?): LiveData<List<Follower>> //= usersApi.getUsers(currentFollowSlug)
//    {
//        val disposable = usersApi.getUsers()
//                .subscribeOn(schedulerProvider.io())
//                .observeOn(schedulerProvider.ui())
//                .doOnSubscribe { status.value = Status.LOADING }
//                .subscribe({ response ->
//                    followers.value = response.response
//                    status.value = Status.SUCCESS
//                }, { t: Throwable? ->
//                    followers.value = emptyList()
//                    status.value = Status.ERROR
//                })
//        disposables.add(disposable)
//    }
//
//    fun getStatus(): LiveData<Status> {
//        return status
//    }
//    fun refresh() {
//        followers.value = emptyList()
//        getFollowers()
//    }
//
//    fun retry() {
//        getFollowers()
//    }
//
//    override fun onCleared() {
//        super.onCleared()
//        disposables.clear()
//    }
//
//}