package com.robotogram.followerslist.api

import com.robotogram.followerslist.data.models.RobotsResponse
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface RobotsApi {

    @GET("/api/")
    fun getRobots(@Query("page") page: Int? = null,
                  @Query("results") numberOfResults: Int? = 20,
                  @Query("exc") excludedFields: String? = "login",
                  @Query("seed") seed: String? = "abc"
                 )
            : Observable<RobotsResponse>

    companion object {
        private const val BASE_URL = "https://randomuser.me/api/"
        val instance by lazy { create() }

        private fun create(url: String = BASE_URL): RobotsApi {
            return Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(MoshiConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
                    .create(RobotsApi::class.java)
        }
    }
}