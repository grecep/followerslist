package com.robotogram.followerslist.data.models

import com.squareup.moshi.Json
import java.io.Serializable

data class Id(
        @field:Json(name = "name") val name: String,
        @field:Json(name = "value") val value: String
) : Serializable