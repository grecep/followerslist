package com.robotogram.followerslist.data.models

import com.squareup.moshi.Json

data class RobotsResponse(
        @field:Json(name = "results") val robots: List<Robot>,
        @field:Json(name = "info") val info: Info
)