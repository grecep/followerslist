package com.robotogram.followerslist.data.models

import com.squareup.moshi.Json
import java.io.Serializable

data class Picture(
    @field:Json(name = "large") val large: String,
    @field:Json(name = "medium") val medium: String,
    @field:Json(name = "thumbnail") val thumbnail: String
) : Serializable