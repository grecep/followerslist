package com.robotogram.followerslist.data.models

import com.squareup.moshi.Json
import java.io.Serializable

data class Robot (
    @field:Json(name = "gender") val gender: String,
    @field:Json(name = "name") val name: Name,
    @field:Json(name = "location") val location: Location,
    @field:Json(name = "email") val email: String,
    @field:Json(name = "dob") val dob: Dob,
    @field:Json(name = "registered") val registered: Registered,
    @field:Json(name = "phone") val phone: String,
    @field:Json(name = "cell") val cell: String,
    @field:Json(name = "id") val id: Id,
    @field:Json(name = "picture") val picture: Picture,
    @field:Json(name = "nat") val nat: String
) : Serializable {
    fun getProfileImage() = "https://robohash.org/${id.value}.png?bgset=bg1&size=200x200"
    fun getFullName() = "${name.last.capitalize()} ${name.first.capitalize()}"
    fun getDescription() = "${location.city.capitalize()} ${location.state.capitalize()} $nat"
    fun getTransitionName() = name.first + name.last
}