package com.robotogram.followerslist.data.models

import com.squareup.moshi.Json
import java.io.Serializable

data class Timezone(
    @field:Json(name = "offset") val offset: String,
    @field:Json(name = "description") val description: String
) : Serializable