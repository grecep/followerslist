package com.robotogram.followerslist.data.models

import com.squareup.moshi.Json
import java.io.Serializable

data class Name(
    @field:Json(name = "title") val title: String,
    @field:Json(name = "first") val first: String,
    @field:Json(name = "last") val last: String
) : Serializable