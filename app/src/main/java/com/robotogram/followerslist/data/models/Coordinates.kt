package com.robotogram.followerslist.data.models

import com.squareup.moshi.Json
import java.io.Serializable

data class Coordinates(
    @field:Json(name = "latitude") val latitude: String,
    @field:Json(name = "longitude") val longitude: String
) : Serializable