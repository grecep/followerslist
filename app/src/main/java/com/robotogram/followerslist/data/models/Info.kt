package com.robotogram.followerslist.data.models

import com.squareup.moshi.Json
import java.io.Serializable

data class Info(
        @field:Json(name = "seed") val seed: String,
        @field:Json(name = "results") val results: Int,
        @field:Json(name = "page") val page: Int,
        @field:Json(name = "version") val version: String
) : Serializable