package com.robotogram.followerslist.data

import com.squareup.moshi.Json
import java.io.Serializable

data class FollowersResponse(
    @field:Json(name = "response") val response: List<Follower>
)

data class Follower(@field:Json(name = "slug") val slug: String,
    @field:Json(name = "firstname") val firstname: String,
    @field:Json(name = "lastname") val lastname: String,
    @field:Json(name = "facebook_id") val facebookId: String,
    @field:Json(name = "role_id") val roleId: Int,
    @field:Json(name = "primary_pos_id") val primaryPosId: String?,
    @field:Json(name = "country_id") val countryId: Int,
    @field:Json(name = "gender") val gender: String,
    @field:Json(name = "email") val email: String,
    @field:Json(name = "profile_picture") val profilePicture: String,
    @field:Json(name = "google_id") val googleId: String,
    @field:Json(name = "digits_id") val digitsId: String,
    @field:Json(name = "phone_number") val phoneNumber: String,
    @field:Json(name = "top_player") val topPlayer: Boolean,
    @field:Json(name = "created_at") val createdAt: String,
    @field:Json(name = "account_kit_id") val accountKitId: String,
    @field:Json(name = "is_coach") val isCoach: Boolean,
    @field:Json(name = "ratings_public") val ratingsPublic: Boolean,
    @field:Json(name = "coach_team_slugs") val coachTeamSlugs: List<String>,
    @field:Json(name = "role") val role: String,
    @field:Json(name = "is_verified") val isVerified: Boolean,
    @field:Json(name = "anonymous") val anonymous: Boolean,
    @field:Json(name = "is_following") val isFollowing: Boolean,
    @field:Json(name = "club") val club: Club,
    @field:Json(name = "team") val team: Team
) : Serializable

data class Club(
    @field:Json(name = "slug") val slug: String,
    @field:Json(name = "name") val name: String,
    @field:Json(name = "logo_url") val logoUrl: String
) : Serializable

data class Team(
    @field:Json(name = "id") val id: Int,
    @field:Json(name = "club_name") val clubName: String,
    @field:Json(name = "crowd_sourced") val crowdSourced: Boolean,
    @field:Json(name = "crowdsourced") val crowdsourced: Boolean,
    @field:Json(name = "display_name") val displayName: String,
    @field:Json(name = "is_crowdsourced") val isCrowdsourced: Boolean,
    @field:Json(name = "league_name") val leagueName: String,
    @field:Json(name = "league_slug") val leagueSlug: String,
    @field:Json(name = "name") val name: String,
    @field:Json(name = "require_permission_to_join") val requirePermissionToJoin: Boolean,
    @field:Json(name = "season") val season: Season,
    @field:Json(name = "slug") val slug: String,
    @field:Json(name = "players_count") val playersCount: Int,
    @field:Json(name = "number_of_coaches") val numberOfCoaches: Int,
    @field:Json(name = "league") val league: League,
    @field:Json(name = "club") val club: Club
) : Serializable

data class League(
    @field:Json(name = "name") val name: String,
    @field:Json(name = "slug") val slug: String,
    @field:Json(name = "id") val id: Int
) : Serializable

data class Season(
    @field:Json(name = "start") val start: String,
    @field:Json(name = "end") val end: String
) : Serializable