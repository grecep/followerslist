package com.robotogram.followerslist.data.models

import com.squareup.moshi.Json
import java.io.Serializable

data class Location(
    @field:Json(name = "street") val street: String,
    @field:Json(name = "city") val city: String,
    @field:Json(name = "state") val state: String,
    @field:Json(name = "postcode") val postcode: String,
    @field:Json(name = "coordinates") val coordinates: Coordinates,
    @field:Json(name = "timezone") val timezone: Timezone
) : Serializable