package com.robotogram.followerslist.data.models

import com.squareup.moshi.Json
import java.io.Serializable

data class Registered(
    @field:Json(name = "date") val date: String,
    @field:Json(name = "age") val age: Int
) : Serializable