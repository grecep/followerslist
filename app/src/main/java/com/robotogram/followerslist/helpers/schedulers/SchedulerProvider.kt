package com.robotogram.followerslist.helpers.schedulers

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers

class SchedulerProvider private constructor() : BaseSchedulerProvider {

    override fun io(): Scheduler {
        return Schedulers.io()
    }

    override fun ui(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    companion object {

        private val INSTANCE: SchedulerProvider by lazy { SchedulerProvider() }

        val instance: SchedulerProvider
            @Synchronized get() {
                return INSTANCE
            }
    }
}