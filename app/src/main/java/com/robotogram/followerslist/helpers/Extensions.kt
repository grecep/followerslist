package com.robotogram.followerslist.helpers

import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.robotogram.followerslist.R

fun ImageView.loadImage(url: String?) {
    Glide.with(this)
            .load(url ?: R.mipmap.ic_launcher)
            .into(this)
}

inline fun View.snack(@StringRes message: Int, length: Int = Snackbar.LENGTH_LONG, f: Snackbar.() -> Unit): Snackbar {
    val snack = Snackbar.make(this, message, length)
    snack.f()
    snack.show()
    return snack
}

fun Snackbar.action(@StringRes action: Int, color: Int? = null, listener: (View) -> Unit) {
    setAction(action, listener)
    color?.let { setActionTextColor(color) }
}