package com.robotogram.followerslist.ui

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.robotogram.followerslist.api.RobotsApi
import com.robotogram.followerslist.data.models.Robot
import com.robotogram.followerslist.data.models.RobotsResponse
import com.robotogram.followerslist.helpers.schedulers.BaseSchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class RobotsListViewModel(private val schedulerProvider: BaseSchedulerProvider, private val robotsApi: RobotsApi) : ViewModel() {

    private var robots: MutableLiveData<List<Robot>> = MutableLiveData()
    private var status: MutableLiveData<Status> = MutableLiveData()
    private var lastPage: MutableLiveData<Int> = MutableLiveData()
    private val disposables = CompositeDisposable()

    fun getFollowers(): LiveData<List<Robot>> {
        if (robots.value == null || robots.value!!.isEmpty()) {
            val disposable = robotsApi.getRobots()
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .doOnSubscribe { status.value = Status.LOADING }
                    .subscribe({ response ->
                        robots.value = response.robots
                        lastPage.value = response.info.page
                        status.value = Status.SUCCESS
                    }, { t: Throwable? ->
                        robots.value = emptyList()
                        status.value = Status.ERROR
                    })
            disposables.add(disposable)
        }
        return robots
    }

    fun getStatus(): LiveData<Status> {
        return status
    }

    fun refresh() {
        robots.value = emptyList()
        getFollowers()
    }

    fun retry() {
        getFollowers()
    }

    fun loadNextPage() {
        if (robots.value == null || (robots.value as List<Robot>).isEmpty()) {
            return
        }

        val lastPageNumber = lastPage.value ?: return

        val disposable = robotsApi.getRobots(page = lastPageNumber)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { status.value = Status.LOADING }
                .subscribe({ response: RobotsResponse? ->
                    val newFollowersList = arrayListOf<Robot>()
                    listOfNotNull(robots.value, response?.robots).forEach {
                        newFollowersList.addAll(it)
                    }
                    robots.value = newFollowersList
                    status.value = Status.SUCCESS
                }, { t: Throwable? ->
                    status.value = Status.ERROR
                })
        disposables.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}