package com.robotogram.followerslist.ui

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.robotogram.followerslist.api.RobotsApi
import com.robotogram.followerslist.helpers.schedulers.BaseSchedulerProvider

class FollowersViewModelFactory(private val schedulerProvider: BaseSchedulerProvider, private val robotsApi: RobotsApi) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(RobotsListViewModel::class.java)) {
            return RobotsListViewModel(schedulerProvider, robotsApi) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}