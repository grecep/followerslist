package com.robotogram.followerslist.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.os.Build
import android.support.transition.*
import com.robotogram.followerslist.R
import com.robotogram.followerslist.data.models.Robot


class FollowersListActivity : AppCompatActivity(), OnFollowerClickedListener {

    private val fragmentContainer = R.id.fragment_container

    companion object {
        const val followerBundleKey = "followerBundleKey"
        const val imageTransitionName = "imageTransitionName"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_followers)

        if (savedInstanceState != null) {
            return
        }

        val homeFragment = RobotsListFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(fragmentContainer, homeFragment)
        fragmentTransaction.commit()
    }

    private fun openDetailsFragment(robot: Robot, sharedViewName: String, sharedView: View) {
        val detailsFragment = FollowerDetailFragment()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            detailsFragment.sharedElementEnterTransition = DetailsTransition()
            detailsFragment.enterTransition = Fade()
            detailsFragment.sharedElementReturnTransition = DetailsTransition()
        }

        detailsFragment.arguments = Bundle().apply { putSerializable(followerBundleKey, robot) }
        supportFragmentManager.beginTransaction()
                .addSharedElement(sharedView, sharedViewName)
                .replace(fragmentContainer, detailsFragment)
                .addToBackStack(null)
                .commit()
    }

    override fun onFollowerClicked(robot: Robot, sharedViewName: String, sharedView: View) {
        openDetailsFragment(robot, sharedViewName, sharedView)
    }
}

interface OnFollowerClickedListener {
    fun onFollowerClicked(robot: Robot, sharedViewName: String, sharedView: View)
}

class DetailsTransition : TransitionSet() {
    init {
        ordering = ORDERING_TOGETHER
        addTransition(ChangeBounds())
                .addTransition(ChangeTransform())
                .addTransition(ChangeImageTransform())
    }
}