package com.robotogram.followerslist.ui

import android.os.Build
import android.view.View
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.robotogram.followerslist.R
import com.robotogram.followerslist.data.models.Robot
import com.robotogram.followerslist.helpers.loadImage
import kotlinx.android.synthetic.main.item_follower.view.*

class RobotItem(val robot: Robot) : AbstractItem<RobotItem, RobotItem.ViewHolder>() {

    override fun getType() = R.id.item_follower

    override fun getLayoutRes() = R.layout.item_follower

    override fun getViewHolder(view: View) = ViewHolder(view)

    class ViewHolder(view: View) : FastAdapter.ViewHolder<RobotItem>(view) {

        override fun bindView(item: RobotItem, payloads: List<Any>) {
            val robot = item.robot
            itemView.playerNameTextView.text = robot.getFullName()
            itemView.playerProfileImageView.background = null
            itemView.playerProfileImageView.loadImage(robot.getProfileImage())
            itemView.playerTeamTextView.text = robot.getDescription()

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                itemView.playerProfileImageView.transitionName = "transition" + robot.getTransitionName()
            }
        }

        override fun unbindView(item: RobotItem?) {}
    }
}