package com.robotogram.followerslist.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.transition.Fade
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.IAdapter
import com.mikepenz.fastadapter.IItem
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter_extensions.items.ProgressItem
import com.mikepenz.fastadapter_extensions.scroll.EndlessRecyclerOnScrollListener
import com.robotogram.followerslist.R
import com.robotogram.followerslist.api.RobotsApi
import com.robotogram.followerslist.data.models.Robot
import com.robotogram.followerslist.helpers.action
import com.robotogram.followerslist.helpers.schedulers.SchedulerProvider
import com.robotogram.followerslist.helpers.snack

class RobotsListFragment : Fragment() {

    private val offsetFromBottomToLoadNext = 5 // number of items remaining below screen when we should start loading

    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private var snackbar: Snackbar? = null

    private val itemAdapter by lazy { ItemAdapter<RobotItem>() }
    private val footerAdapter by lazy { ItemAdapter<IItem<*, *>>() }
    private lateinit var robotsListViewModel: RobotsListViewModel
    private lateinit var scrollListener: EndlessRecyclerOnScrollListener

    private val adapter by lazy {
        FastAdapter.with<IItem<*, *>, IAdapter<out IItem<*, *>>>(listOf(itemAdapter, footerAdapter))
                .withOnClickListener { view, _, item, position ->
                    (item as? RobotItem)?.let {
                        openDetailsFragment(item.robot, view!!.findViewById<ImageView>(R.id.playerProfileImageView))
                    }
                    false
                }
    }

    private fun openDetailsFragment(robot: Robot, view: View) {
        exitTransition = Fade()
        (activity as? OnFollowerClickedListener)?.onFollowerClicked(robot, FollowersListActivity.imageTransitionName, view)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_followers_list, container, false)

        recyclerView = view.findViewById(R.id.recycler_view)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.itemAnimator = null

        swipeRefreshLayout = view.findViewById(R.id.swipe_container)

        val followersViewModelFactory = FollowersViewModelFactory(SchedulerProvider.instance, RobotsApi.instance)
        robotsListViewModel = ViewModelProviders.of(this, followersViewModelFactory).get(RobotsListViewModel::class.java)

        scrollListener = object : EndlessRecyclerOnScrollListener(recyclerView.layoutManager, offsetFromBottomToLoadNext, footerAdapter) {
            override fun onLoadMore(currentPage: Int) {
                // Scroll callbacks mightbe run during a measure & layout pass where you cannot change theRecyclerView data
                Handler().post {
                    robotsListViewModel.loadNextPage()
                }
            }
        }

        swipeRefreshLayout.setOnRefreshListener {
            scrollListener.disable()
            scrollListener.resetPageCount()
            robotsListViewModel.refresh()
        }
        recyclerView.addOnScrollListener(scrollListener)

        robotsListViewModel.getFollowers().observe(this, Observer { followers ->
            showData(followers ?: emptyList())
        })

        robotsListViewModel.getStatus().observe(this, Observer {
            it?.let {
                showStatus(it)
            }
        })

        return view
    }

    private fun showError() {
        view?.let {
            snackbar = it.snack(R.string.no_data, Snackbar.LENGTH_INDEFINITE) {
                action(R.string.retry) {
                    robotsListViewModel.retry()
                }
            }
        }
    }

    private fun hideError() {
        snackbar?.let {
            if (it.isShown) {
                it.dismiss()
            }
        }
    }

    private fun showData(followers: List<Robot>) {
        val items = followers.map { RobotItem(it) }
        itemAdapter.set(items)
        adapter.notifyAdapterDataSetChanged()
        swipeRefreshLayout.isRefreshing = false
        if (items.isNotEmpty()) {
            scrollListener.enable()
        }
    }

    private fun showProgressBar() {
        footerAdapter.set(listOf(ProgressItemWithPadding().withEnabled(false)))
    }

    private fun hideProgressBar() {
        footerAdapter.clear()
    }

    private fun showStatus(status: Status) {
        hideProgressBar()
        hideError()
        when (status) {
            Status.LOADING -> showProgressBar()
            Status.ERROR -> showError()
            Status.SUCCESS -> Unit
        }
    }
}

class ProgressItemWithPadding : ProgressItem() {
    override fun getLayoutRes() = R.layout.progress_bar
}

enum class Status {
    LOADING,
    SUCCESS,
    ERROR
}