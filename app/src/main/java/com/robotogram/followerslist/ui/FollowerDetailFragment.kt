package com.robotogram.followerslist.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.robotogram.followerslist.R
import com.robotogram.followerslist.data.models.Robot
import com.robotogram.followerslist.helpers.loadImage
import kotlinx.android.synthetic.main.item_follower.view.*

class FollowerDetailFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_follower_details, container, false)

        val robot = arguments?.getSerializable(FollowersListActivity.followerBundleKey) as? Robot
        robot?.let {
            view.playerNameTextView.text = robot.getFullName()
            view.playerProfileImageView.loadImage(robot.getProfileImage())
            ViewCompat.setTransitionName(view.playerProfileImageView, FollowersListActivity.imageTransitionName)
            view.playerTeamTextView.text = robot.getDescription()
        }

        return view
    }

}