package com.robotogram.followerslist

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.squareup.moshi.Moshi
import com.robotogram.followerslist.api.RobotsApi
import com.robotogram.followerslist.data.models.Robot
import com.robotogram.followerslist.data.models.RobotsResponse
import com.robotogram.followerslist.helpers.schedulers.TestSchedulerProvider
import com.robotogram.followerslist.ui.RobotsListViewModel
import com.robotogram.followerslist.ui.Status
import io.reactivex.Observable

import org.junit.Test

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see [Testing documentation](http://d.android.com/tools/testing)
 */
class RobotsViewModelUnitTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var robotsApi: RobotsApi
    @Mock
    private lateinit var robotsListObserver: Observer<List<Robot>>
    @Mock
    private lateinit var statusObserver: Observer<Status>

    private lateinit var robotsListViewModel: RobotsListViewModel

    private var robotsResponse: RobotsResponse? = null

    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        val moshi = Moshi.Builder().build()
        val jsonAdapter = moshi.adapter(RobotsResponse::class.java)
        robotsResponse = jsonAdapter.fromJson(RobotsTestData.json)

        robotsListViewModel = RobotsListViewModel(TestSchedulerProvider(), robotsApi)
    }

    @Test
    fun testJsonParsing() {
        assertNotNull(robotsApi)
        assertNotNull(robotsResponse)
        robotsResponse?.let {
            assertEquals(RobotsTestData.listSize, it.robots.size)
            assertEquals(RobotsTestData.firstUserFirstName, it.robots[0].name.first)
            assertEquals(RobotsTestData.firstUserLastName, it.robots[0].name.last)
        }
    }

    @Captor
    private lateinit var captor: ArgumentCaptor<List<Robot>>

    @Test
    fun testFollowersLiveData() {
        whenCalled(robotsApi.getRobots()).thenReturn(Observable.just(robotsResponse))

        val followersLiveData = robotsListViewModel.getFollowers()
        followersLiveData.observeForever(robotsListObserver)

        robotsListViewModel.getFollowers()

        verify(robotsListObserver).onChanged(capture(captor))
        assertNotNull(captor)
        val followersList = captor.value
        assertNotNull(followersList)
        assertEquals(RobotsTestData.listSize, followersList.size)
        assertEquals(RobotsTestData.firstUserFirstName, followersList[0].name.first)
        assertEquals(RobotsTestData.firstUserLastName, followersList[0].name.last)
    }

    @Test
    fun testStatusLiveData() {
        whenCalled(robotsApi.getRobots()).thenReturn(Observable.just(robotsResponse))

        val statusLiveData = robotsListViewModel.getStatus()
        statusLiveData.observeForever(statusObserver)

        robotsListViewModel.getFollowers()

        verify(statusObserver).onChanged(Status.LOADING)
        verify(statusObserver).onChanged(Status.SUCCESS)
    }
}