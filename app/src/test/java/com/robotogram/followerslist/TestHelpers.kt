package com.robotogram.followerslist

import org.mockito.ArgumentCaptor
import org.mockito.Mockito
import org.mockito.stubbing.OngoingStubbing

fun <T> whenCalled(methodCall: T)  = Mockito.`when`(methodCall)

fun <T> capture(argumentCaptor: ArgumentCaptor<T>): T = argumentCaptor.capture()

//fun <T> `when`(methodCall: T): OngoingStubbing<T> {
//    return MOCKITO_CORE.`when`(methodCall)
//}